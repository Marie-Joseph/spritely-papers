;;; Copyright 2022 Christine Lemmer-Webber
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(define-module (test-simple-sealers)
  #:use-module (srfi srfi-64)
  #:use-module (ice-9 regex)
  #:use-module (simple-sealers))

(test-begin "simple-sealers")

#|
#+BEGIN_SRC wisp
  REPL> define-values (our-lunch-seal our-lunch-unseal our-can?)
  _____   make-sealer-triplet
  REPL> define-values (rival-lunch-seal rival-lunch-unseal rival-can?)
  _____   make-sealer-triplet
#+END_SRC
|#

(define-values (our-lunch-seal our-lunch-unseal our-can?)
  (make-sealer-triplet))
(define-values (rival-lunch-seal rival-lunch-unseal rival-can?)
  (make-sealer-triplet))

#|
#+BEGIN_SRC wisp
  REPL> our-lunch-seal 'fried-rice
  ; => #<seal>
#+END_SRC
|#

(define sealed-obj-displayed
  (with-output-to-string (lambda () (display (our-lunch-seal 'fried-rice)))))

(test-assert (string-match "seal" sealed-obj-displayed))

#|
#+BEGIN_SRC wisp
  REPL> define chickpea-lunch
  _____   our-lunch-seal 'chickpea-salad
#+END_SRC
|#

(define chickpea-lunch
  (our-lunch-seal 'chickpea-salad))

#|
#+BEGIN_SRC wisp
  REPL> our-can? chickpea-lunch
  ; => #t (true)
  REPL> our-can?
  _____   rival-lunch-seal 'melted-ice-cream
  ; => #f
#+END_SRC
|#

(test-assert
 (our-can? chickpea-lunch))

(test-assert (not (our-can?
                   (rival-lunch-seal 'melted-ice-cream))))

#|
#+BEGIN_SRC wisp
  REPL> our-lunch-unseal chickpea-lunch
  ; => 'chickpea-salad
#+END_SRC
|#

(test-equal 'chickpea-salad
  (our-lunch-unseal chickpea-lunch))

(test-end "simple-sealers")
